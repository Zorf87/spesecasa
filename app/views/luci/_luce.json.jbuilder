json.extract! luce, :id, :dal, :al, :f1, :f2, :f3, :euro_fornitura, :euro_pagato, :note, :created_at, :updated_at
json.url luce_url(luce, format: :json)
