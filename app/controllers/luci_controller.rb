class LuciController < ApplicationController
  before_action :set_luce, only: [:show, :edit, :update, :destroy]

  # GET /luci
  # GET /luci.json
  def index
    @luci = Luce.all
  end

  # GET /luci/1
  # GET /luci/1.json
  def show
  end

  # GET /luci/new
  def new
    @luce = Luce.new
  end

  # GET /luci/1/edit
  def edit
  end

  # POST /luci
  # POST /luci.json
  def create
    @luce = Luce.new(luce_params)

    @luce.dal = DateTime.strptime(luce_params[:dal]+"-01", "%Y-%m-%d")
    val = luce_params[:al].split("-")
    @luce.al = Date.new(val[0].to_i,val[1].to_i,-1)

    respond_to do |format|
      if @luce.save
        format.html { redirect_to @luce, notice: 'Bolletta Luce inserita correttamente.' }
        format.json { render :show, status: :created, location: @luce }
      else
        format.html { render :new }
        format.json { render json: @luce.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /luci/1
  # PATCH/PUT /luci/1.json
  def update
    respond_to do |format|

      luce_params[:dal] = DateTime.strptime(luce_params[:dal]+"-01", "%Y-%m-%d")
      val = luce_params[:al].split("-")
      luce_params[:al] = Date.new(val[0].to_i,val[1].to_i,-1)

      if @luce.update(luce_params)
        format.html { redirect_to @luce, notice: 'Bolletta Luce aggiornata correttamente.' }
        format.json { render :show, status: :ok, location: @luce }
      else
        format.html { render :edit }
        format.json { render json: @luce.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /luci/1
  # DELETE /luci/1.json
  def destroy
    @luce.destroy
    respond_to do |format|
      format.html { redirect_to luci_url, notice: 'Bolletta Luce eliminata correttamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_luce
      @luce = Luce.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def luce_params
      params.require(:luce).permit(:dal, :al, :f1, :f2, :f3, :euro_fornitura, :euro_pagato, :note)
    end
end
