class HomeController < ApplicationController
  def index
    today = Date.today
    start = Date.new(today.year,1,1)

    @energia_sum_anno = Luce.select("SUM(luci.f1) as sum_f1, SUM(luci.f2) as sum_f2, SUM(luci.f3) as sum_f3, SUM(luci.f1+ luci.f2 +luci.f3) as sum_tot, SUM(luci.euro_fornitura) as sum_fornitura, SUM(luci.euro_pagato) as sum_pagato").where("dal between ? and ? or al between ? and ?","#{start}","#{today}","#{start}","#{today}")[0]
    @energia_avg_anno = Luce.select("avg(luci.f1) as avg_f1, avg(luci.f2) as avg_f2, avg(luci.f3) as avg_f3, avg(luci.f1+ luci.f2 +luci.f3) as avg_tot, avg(luci.euro_fornitura) as avg_fornitura, avg(luci.euro_pagato) as avg_pagato").where("dal between ? and ? or al between ? and ?","#{start}","#{today}","#{start}","#{today}")[0]
  end
end
