$(function() {
    $(document).ready(function() {
        caricaGrafico();
    });
});

function caricaGrafico() {
    $.getJSON('/luci.json', function(data) {

        var data_f1 = [];
        var data_f2 = [];
        var data_f3 = [];
        var data_tot = [];
        var categories = [];
        $.map(data, function(luce, i) {
            var label;
            var dal = moment(luce.dal);
            var al = moment(luce.al);
            if (dal.year() == al.year()) {
                label = dal.format('MMM') + " - " + al.format('MMM YY');
            } else if (dal.year() < al.year()) {
                label = dal.format('MMM YY') + " - " + al.format('MMM YY');
            }

            categories.push([i, label]);
            data_f1.push([i, luce.f1]);
            data_f2.push([i, luce.f2]);
            data_f3.push([i, luce.f3]);
            data_tot.push([i, luce.f1 + luce.f2 + luce.f3]);
        });

        var options = {
            legend: {
                show: true,
                //labelFormatter: null or(fn: string, series object - > string)
                //labelBoxBorderColor: color
                //noColumns: number
                position: "nw",
                //or "nw"
                //or "se"
                //or "sw"
                //margin: number of pixels or[x margin, y margin]
                //backgroundColor: null or color
                //backgroundOpacity: number between 0 and 1
                //container: null or jQuery object / DOM element / jQuery expression
                //sorted: null / false, true, "ascending", "descending", "reverse", or a comparator
            },
            series: {
                lines: {
                    show: true
                },
                points: {
                    show: true,
                    radius: 4
                },
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.5
                }
            },
            grid: {
                borderColor: '#eee',
                borderWidth: 1,
                hoverable: true,
                backgroundColor: '#fcfcfc'
            },
            tooltip: true,
            tooltipOpts: {
                content: function(label, x, y) { return y + ' KWh'; }
            },
            xaxis: {
                tickColor: '#fcfcfc',
                ticks: categories
            },
            yaxis: {
                min: 0,
                //max: 150, // optional: use it for a clear represetation
                tickColor: '#eee',
                //position: 'right' or 'left',
                tickFormatter: function(v) {
                    return v /* + ' visitors'*/ ;
                }
            },
            shadowSize: 0
        };

        $.plot($('#plot_energia'), [{
                label: "F1",
                color: "#dc3545",
                data: data_f1
            },
            {
                label: "F2",
                color: "#ffc107",
                data: data_f2
            },
            {
                label: "F3",
                color: "#007bff",
                data: data_f3
            },
            {
                label: "Tot",
                color: "#333",
                data: data_tot
            }
        ], options);
    });
}