class CreateLuci < ActiveRecord::Migration
  def change
    create_table :luci do |t|
      t.date :dal
      t.date :al
      t.integer :f1
      t.integer :f2
      t.integer :f3
      t.float :euro_fornitura
      t.float :euro_pagato
      t.string :note

      t.timestamps null: false
    end
  end
end
