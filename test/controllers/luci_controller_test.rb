require 'test_helper'

class LuciControllerTest < ActionController::TestCase
  setup do
    @luce = luci(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:luci)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create luce" do
    assert_difference('Luce.count') do
      post :create, luce: { al: @luce.al, dal: @luce.dal, euro_fornitura: @luce.euro_fornitura, euro_pagato: @luce.euro_pagato, f1: @luce.f1, f2: @luce.f2, f3: @luce.f3, note: @luce.note }
    end

    assert_redirected_to luce_path(assigns(:luce))
  end

  test "should show luce" do
    get :show, id: @luce
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @luce
    assert_response :success
  end

  test "should update luce" do
    patch :update, id: @luce, luce: { al: @luce.al, dal: @luce.dal, euro_fornitura: @luce.euro_fornitura, euro_pagato: @luce.euro_pagato, f1: @luce.f1, f2: @luce.f2, f3: @luce.f3, note: @luce.note }
    assert_redirected_to luce_path(assigns(:luce))
  end

  test "should destroy luce" do
    assert_difference('Luce.count', -1) do
      delete :destroy, id: @luce
    end

    assert_redirected_to luci_path
  end
end
